FROM registry.gitlab.com/multitech-osp/dockerfiles/php/7.4-fpm-alpine-nginx

# Install required PHP extensions and all their prerequisites available via apt.
RUN chmod uga+x /usr/bin/install-php-extensions \
    && sync \
    && install-php-extensions xdebug

COPY ./php/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

EXPOSE 9003 9000
